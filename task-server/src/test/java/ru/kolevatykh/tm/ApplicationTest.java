package ru.kolevatykh.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Test;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.service.ProjectService;
import ru.kolevatykh.tm.service.TaskService;
import ru.kolevatykh.tm.service.UserService;
import ru.kolevatykh.tm.util.PasswordHashUtil;

import static org.junit.Assert.*;

public class ApplicationTest {

    @Test
    public void shouldFindUserByLogin() throws Exception {
        @NotNull final UserService userService = new UserService();
        assertNotNull(userService);

        @Nullable User user = userService.findOneByLogin("demo");
        assertNull(user);

        @NotNull final User test = new User("demo", PasswordHashUtil.getPasswordHash("demo"), RoleType.USER);
        userService.persist(test);
        assertNotNull(test);

        user = userService.findOneByLogin("demo");
        assertNotNull(user);

        userService.remove(user.getId());
        user = userService.findOneByLogin("demo");
        assertNull(user);
    }

    @Test
    public void shouldAssignTaskToProject() throws Exception {
        @NotNull final UserService userService = new UserService();
        @NotNull final User test = new User("test", PasswordHashUtil.getPasswordHash("test"), RoleType.ADMIN);

        userService.persist(test);
        assertNotNull(test);

        @Nullable User user = userService.findOneByLogin("test");
        assertNotNull(user);

        @NotNull final ProjectService projectService = new ProjectService();
        @NotNull final TaskService taskService = new TaskService();

        @NotNull final Project project = new Project("demo project", "desc project 1", null, null);
        project.setUserId(user.getId());
        projectService.persist(project);

        @NotNull final Task task = new Task("demo task", "desc task 1", null, null);
        task.setUserId(user.getId());
        taskService.persist(task);

        assertNotNull(taskService.findOneByName(task.getUserId(), "demo task"));
        assertNotNull(projectService.findOneByName(project.getUserId(), "demo project"));

        task.setProjectId(project.getId());
        taskService.merge(task);
        assertNotNull(task);

        @Nullable final Task testTask = taskService.findOneById(test.getId(), task.getId());
        assertNotNull(testTask.getProjectId());

        taskService.remove(user.getId(), task.getId());
        assertNull(taskService.findOneByName(user.getId(), "demo task"));
        projectService.remove(user.getId(), project.getId());
        assertNull(projectService.findOneByName(user.getId(), "demo project"));
        userService.remove(user.getId());
        assertNull(userService.findOneByLogin("test"));
    }
}
