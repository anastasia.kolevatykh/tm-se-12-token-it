package ru.kolevatykh.tm.util;

import org.jetbrains.annotations.NotNull;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DatabaseConnectionUtil {

    @NotNull
    public static Connection getConnection() throws Exception {
        @NotNull final Properties props = new Properties();
        InputStream is = DateFormatterUtil.class.getResourceAsStream("/META-INF/mySql_db.properties");
        props.load(is);
        is.close();
        @NotNull final String driver = props.getProperty("jdbc.driver");
        if (driver != null) {
            Class.forName(driver) ;
        }
        @NotNull final String url = props.getProperty("jdbc.url");
        @NotNull final String username = props.getProperty("jdbc.username");
        @NotNull final String password = props.getProperty("jdbc.password");
        return DriverManager.getConnection(url, username, password);
    }
}
