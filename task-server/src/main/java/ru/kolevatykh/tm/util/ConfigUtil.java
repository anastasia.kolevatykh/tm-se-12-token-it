package ru.kolevatykh.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

public class ConfigUtil {

    @NotNull
    public static String getKey() throws Exception {
        @NotNull final String key = getPropertiesFile().getProperty("key");
        return key;
    }

    @NotNull
    public static String getSalt() throws Exception {
        @NotNull final String salt = getPropertiesFile().getProperty("salt");
        return salt;
    }

    @NotNull
    private static Properties getPropertiesFile() throws Exception {
        @NotNull final Properties props = new Properties();
        InputStream is = ConfigUtil.class.getResourceAsStream("/META-INF/config.properties");
        props.load(is);
        is.close();
        return props;
    }
}
