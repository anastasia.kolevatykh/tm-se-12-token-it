package ru.kolevatykh.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

public class AESUtil {
    @Nullable
    private static SecretKeySpec secretKeySpec;
    @Nullable
    private static byte[] key;

    public static void setKey(@NotNull final String myKey) throws Exception {
        MessageDigest sha = null;
        key = myKey.getBytes("UTF-8");
        sha = MessageDigest.getInstance("SHA-1");
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16);
        secretKeySpec = new SecretKeySpec(key, "AES");
    }

    @NotNull
    public static String encrypt(@NotNull final String strToEncrypt,
                                 @NotNull final String secret)
            throws Exception {
        setKey(secret);
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        byte[] data = cipher.doFinal(strToEncrypt.getBytes("UTF-8"));
        return Base64.getEncoder().encodeToString(data);
    }

    @NotNull
    public static String decrypt(@NotNull final String strToDecrypt,
                                 @NotNull final String secret)
            throws Exception {
        setKey(secret);
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
        byte[] data = Base64.getDecoder().decode(strToDecrypt);
        return new String(cipher.doFinal(data));
    }
}
