package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Task;
import java.util.List;

public interface ITaskService extends IProjectTaskService<Task> {

    @Nullable List<Task> findAll() throws Exception;

    @Nullable List<Task> findAllByUserId(@Nullable String userId) throws Exception;

    @Nullable Task findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable Task findOneByName(@Nullable String userId, @Nullable String name) throws Exception;

    void persist(@Nullable final Task task) throws Exception;

    void merge(@Nullable final Task task) throws Exception;

    void remove(@Nullable String userId, @Nullable String id) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    void removeTasksWithProjectId(@Nullable String userId) throws Exception;

    void removeProjectTasks(@Nullable String userId, @Nullable String projectId) throws Exception;

    @Nullable List<Task> findTasksByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    @Nullable List<Task> findTasksWithoutProject(@Nullable String userId) throws Exception;

    @Nullable List<Task> findAllSortedByStartDate(@Nullable final String userId) throws Exception;

    @Nullable List<Task> findAllSortedByFinishDate(@Nullable final String userId) throws Exception;

    @Nullable List<Task> findAllSortedByStatus(@Nullable final String userId) throws Exception;

    @Nullable List<Task> findAllBySearch(@Nullable final String userId, @Nullable final String search) throws Exception;
}
