package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Task;
import java.util.List;

public interface ITaskRepository extends IProjectTaskRepository<Task> {

    @NotNull List<Task> findAll() throws Exception;

    @NotNull List<Task> findAllByUserId(@NotNull String userId) throws Exception;

    @Nullable Task findOneById(@NotNull String userId, @NotNull String id) throws Exception;

    @Nullable Task findOneByName(@NotNull String userId, @NotNull String name) throws Exception;

    void persist(@NotNull final Task task) throws Exception;

    void merge(@NotNull final Task task) throws Exception;

    void remove(@NotNull String userId, @NotNull String id) throws Exception;

    void removeAll(@NotNull String userId) throws Exception;

    void removeTasksWithProjectId(@NotNull String userId) throws Exception;

    void removeProjectTasks(@NotNull String userId, @NotNull String projectId) throws Exception;

    @NotNull List<Task> findTasksByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

    @NotNull List<Task> findTasksWithoutProject(@NotNull String userId) throws Exception;

    @NotNull List<Task> findAllSortedByStartDate(@NotNull final String userId) throws Exception;

    @NotNull List<Task> findAllSortedByFinishDate(@NotNull final String userId) throws Exception;

    @NotNull List<Task> findAllSortedByStatus(@NotNull final String userId) throws Exception;
}
