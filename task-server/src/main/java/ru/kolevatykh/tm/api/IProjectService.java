package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Project;
import java.util.List;

public interface IProjectService extends IProjectTaskService<Project> {

    @Nullable List<Project> findAll() throws Exception;

    @Nullable List<Project> findAllByUserId(@Nullable String userId) throws Exception;

    @Nullable Project findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable Project findOneByName(@Nullable String userId, @Nullable String name) throws Exception;

    void persist(@Nullable final Project project) throws Exception;

    void merge(@Nullable final Project project) throws Exception;

    void remove(@Nullable String userId, @Nullable String id) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @Nullable List<Project> findAllSortedByStartDate(@Nullable final String userId) throws Exception;

    @Nullable List<Project> findAllSortedByFinishDate(@Nullable final String userId) throws Exception;

    @Nullable List<Project> findAllSortedByStatus(@Nullable final String userId) throws Exception;
}