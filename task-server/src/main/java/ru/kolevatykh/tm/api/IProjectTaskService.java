package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.Nullable;
import java.util.List;

public interface IProjectTaskService<T> {

    @Nullable List<T> findAll() throws Exception;

    @Nullable List<T> findAllByUserId(@Nullable String userId) throws Exception;

    @Nullable T findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable T findOneByName(@Nullable String userId, @Nullable String name) throws Exception;

    void persist(@Nullable final T entity) throws Exception;

    void merge(@Nullable final T entity) throws Exception;

    void remove(@Nullable String userId, @Nullable String id) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @Nullable List<T> findAllSortedByCreateDate(@Nullable final String userId) throws Exception;

    @Nullable List<T> findAllSortedByStartDate(@Nullable final String userId) throws Exception;

    @Nullable List<T> findAllSortedByFinishDate(@Nullable final String userId) throws Exception;

    @Nullable List<T> findAllSortedByStatus(@Nullable final String userId) throws Exception;

    @Nullable List<T> findAllBySearch(@Nullable final String userId, @Nullable final String search) throws Exception;
}
