package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.User;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull List<User> findAll() throws Exception;

    @Nullable User findOneById(@NotNull String id) throws Exception;

    @Nullable User findOneByLogin(@NotNull String login) throws Exception;

    void persist(@NotNull final User user) throws Exception;

    void merge(@NotNull final User user) throws Exception;

    void remove(@NotNull String id) throws Exception;

    void removeAll() throws Exception;
}
