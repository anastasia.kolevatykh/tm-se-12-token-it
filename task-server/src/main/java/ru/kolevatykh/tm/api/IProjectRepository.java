package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Project;
import java.util.List;

public interface IProjectRepository extends IProjectTaskRepository<Project> {

    @NotNull List<Project> findAll() throws Exception;

    @NotNull List<Project> findAllByUserId(@NotNull String userId) throws Exception;

    @Nullable Project findOneById(@NotNull String userId, @NotNull String id) throws Exception;

    @Nullable Project findOneByName(@NotNull String userId, @NotNull String name) throws Exception;

    void persist(@NotNull final Project project) throws Exception;

    void merge(@NotNull final Project project) throws Exception;

    void remove(@NotNull String userId, @NotNull String id) throws Exception;

    void removeAll(@NotNull String userId) throws Exception;

    @NotNull List<Project> findAllSortedByStartDate(@NotNull final String userId) throws Exception;

    @NotNull List<Project> findAllSortedByFinishDate(@NotNull final String userId) throws Exception;

    @NotNull List<Project> findAllSortedByStatus(@NotNull final String userId) throws Exception;

    @NotNull List<Project> findAllBySearch(@NotNull final String userId, @NotNull final String search) throws Exception;
}
