package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.entity.Session;

public interface ISessionRepository extends IRepository<Session> {

    void removeAllByUserId(@NotNull final String userId) throws Exception;
}
