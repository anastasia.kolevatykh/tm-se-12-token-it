package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<T> {

    @NotNull List<T> findAll() throws Exception;

    @Nullable T findOneById(@NotNull String id) throws Exception;

    void persist(@NotNull T entity) throws Exception;

    void merge(@NotNull T entity) throws Exception;

    void remove(@NotNull String id) throws Exception;

    void removeAll() throws Exception;
}
