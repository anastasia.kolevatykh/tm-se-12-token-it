package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.List;

public interface IProjectTaskRepository<T> {

    @NotNull List<T> findAll() throws Exception;

    @NotNull List<T> findAllByUserId(@NotNull String userId) throws Exception;

    @Nullable T findOneById(@NotNull String userId, @NotNull String id) throws Exception;

    @Nullable T findOneByName(@NotNull String userId, @NotNull String name) throws Exception;

    void persist(@NotNull final T entity) throws Exception;

    void merge(@NotNull final T entity) throws Exception;

    void remove(@NotNull String userId, @NotNull String id) throws Exception;

    void removeAll(@NotNull String userId) throws Exception;

    @Nullable List<T> findAllSortedByCreateDate(@Nullable final String userId) throws Exception;

    @NotNull List<T> findAllSortedByStartDate(@NotNull final String userId) throws Exception;

    @NotNull List<T> findAllSortedByFinishDate(@NotNull final String userId) throws Exception;

    @NotNull List<T> findAllSortedByStatus(@NotNull final String userId) throws Exception;

    @NotNull List<T> findAllBySearch(@NotNull final String userId, @NotNull final String search) throws Exception;
}
