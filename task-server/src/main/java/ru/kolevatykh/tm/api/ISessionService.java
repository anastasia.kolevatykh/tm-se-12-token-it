package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.User;

public interface ISessionService extends IService<Session> {

    @Nullable ISessionRepository getSessionRepository();

    @Nullable User getUser(@Nullable Session session) throws Exception;

    @Nullable Session openAuth(@Nullable String login, @Nullable String password) throws Exception;

    @Nullable Session openReg(@Nullable String login, @Nullable String password) throws Exception;

    void validate(@Nullable Session session) throws Exception;

    void removeAllByUserId(@Nullable final String userId) throws Exception;
}
