package ru.kolevatykh.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.enumerate.RoleType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.PROPERTY)
public final class User extends AbstractEntity implements Serializable {

    @NonNull
    private String login;

    @NotNull
    private String passwordHash;

    @NonNull
    private RoleType roleType;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @Nullable
    private String phone;

    private Boolean locked = false;

    public User(@NotNull final String login,
                @NotNull final String password,
                @NotNull final RoleType roleType) {
        super();
        this.login = login;
        this.passwordHash = password;
        this.roleType = roleType;
    }

    @Override
    public String toString() {
        return "id: '" + id + '\'' +
                ", login: '" + login + '\'' +
                ", roleType: " + roleType +
                ", isLocked: " + locked;
    }
}
