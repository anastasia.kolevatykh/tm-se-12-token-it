package ru.kolevatykh.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.ConfigUtil;
import ru.kolevatykh.tm.util.SignatureUtil;

@Setter
@Getter
@NoArgsConstructor
public final class Session extends AbstractEntity implements Cloneable {

    @Nullable
    private String signature;

    @NotNull
    private Long timestamp = System.currentTimeMillis();

    @NotNull
    private String userId;

    @NotNull
    private RoleType roleType;

    public static String generateSignature(@NotNull final Session session) throws Exception {
        @NotNull final Session tempSession = (Session) session.clone();
        tempSession.setSignature(null);
        return SignatureUtil.sign(tempSession,  ConfigUtil.getSalt(), 13);
    }
}
