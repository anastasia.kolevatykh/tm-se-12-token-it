package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IUserRepository;
import ru.kolevatykh.tm.constant.FieldConst;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    private User fetch(@Nullable final ResultSet row) throws Exception {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString(FieldConst.ID));
        user.setLogin(row.getString(FieldConst.LOGIN));
        user.setPasswordHash(row.getString(FieldConst.PASSWORD_HASH));
        user.setRoleType(RoleType.valueOf(row.getString(FieldConst.ROLE_TYPE)));
        user.setEmail(row.getString(FieldConst.EMAIL));
        user.setFirstName(row.getString(FieldConst.FIRST_NAME));
        user.setLastName(row.getString(FieldConst.LAST_NAME));
        user.setMiddleName(row.getString(FieldConst.MIDDLE_NAME));
        user.setPhone(row.getString(FieldConst.PHONE));
        user.setLocked(row.getBoolean(FieldConst.LOCKED));
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_user";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. List<User> findAll]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<User> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        statement.close();
        return result;
    }

    @Nullable
    @Override
    public User findOneById(@NotNull String id) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_user WHERE id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. User findOneById]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @Nullable User result = null;
        if (resultSet.next()) result = fetch(resultSet);
        resultSet.close();
        statement.close();
        return result;
    }

    @Override
    public void persist(@NotNull final User user) throws Exception {
        @NotNull final String query = "INSERT INTO `my-task-manager_db`.app_user VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. User persist]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setString(4, user.getRoleType().displayName());
        statement.setString(5, user.getEmail());
        statement.setString(6, user.getFirstName());
        statement.setString(7, user.getLastName());
        statement.setString(8, user.getMiddleName());
        statement.setString(9, user.getPhone());
        statement.setBoolean(10, user.getLocked());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void merge(@NotNull final User user) throws Exception {
        @NotNull final String query = "UPDATE `my-task-manager_db`.app_user SET login = ?, "
                + "password_hash = ?, role_type = ?, email = ?, first_name = ?,"
                + "last_name = ?, middle_name = ?, phone = ?, locked = ? WHERE id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. User merge]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, user.getLogin());
        statement.setString(2, user.getPasswordHash());
        statement.setString(3, user.getRoleType().displayName());
        statement.setString(4, user.getEmail());
        statement.setString(5, user.getFirstName());
        statement.setString(6, user.getLastName());
        statement.setString(7, user.getMiddleName());
        statement.setString(8, user.getPhone());
        statement.setBoolean(9, user.getLocked());
        statement.setString(10, user.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void remove(@NotNull final String id) throws Exception {
        @NotNull final String query = "DELETE FROM `my-task-manager_db`.app_user WHERE id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. User remove]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String query = "DELETE FROM `my-task-manager_db`.app_user;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. User removeAll]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    public final User findOneByLogin(@NotNull final String login) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_user WHERE login = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. User findOneByLogin]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @Nullable User result = null;
        if (resultSet.next()) result = fetch(resultSet);
        resultSet.close();
        statement.close();
        return result;
    }
}
