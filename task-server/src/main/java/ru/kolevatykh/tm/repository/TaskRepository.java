package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ITaskRepository;
import ru.kolevatykh.tm.constant.FieldConst;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.enumerate.StatusType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

public final class TaskRepository extends AbstractProjectTaskRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    private Task fetch(@Nullable final ResultSet row) throws Exception {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString(FieldConst.ID));
        task.setUserId(row.getString(FieldConst.USER_ID));
        task.setProjectId(row.getString(FieldConst.PROJECT_ID));
        task.setName(row.getString(FieldConst.NAME));
        task.setDescription(row.getString(FieldConst.DESCRIPTION));
        task.setCreateDate(row.getDate(FieldConst.CREATE_DATE));
        task.setStartDate(row.getDate(FieldConst.START_DATE));
        task.setFinishDate(row.getDate(FieldConst.FINISH_DATE));
        task.setStatusType(StatusType.valueOf(row.getString(FieldConst.STATUS_TYPE)));
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_task";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. List<Task> findAll]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@NotNull final String userId) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = ?";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. List<Task> findAllByUserId]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    public void persist(@NotNull final Task task) throws Exception {
        @NotNull final String query = "INSERT INTO `my-task-manager_db`.app_task VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Task persist]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, task.getId());
        statement.setString(2, task.getUserId());
        statement.setString(3, task.getProjectId());
        statement.setString(4, task.getName());
        statement.setString(5, task.getDescription());
        statement.setDate(6, new java.sql.Date(task.getCreateDate().getTime()));
        if (task.getStartDate() != null)
            statement.setDate(7, new java.sql.Date(task.getStartDate().getTime()));
        else
            statement.setDate(7, null);
        if (task.getFinishDate() != null)
            statement.setDate(8, new java.sql.Date(task.getFinishDate().getTime()));
        else
            statement.setDate(8, null);
        statement.setString(9, task.getStatusType().displayName());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void merge(@NotNull final Task task) throws Exception {
        @NotNull final String query = "UPDATE `my-task-manager_db`.app_task SET user_id = ?, name = ?, description = ?," +
                "create_date = ?, start_date = ?, finish_date = ?, status_type = ?, project_id = ? WHERE id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Task merge]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, task.getUserId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setDate(4, new java.sql.Date(task.getCreateDate().getTime()));
        if (task.getStartDate() != null)
            statement.setDate(5, new java.sql.Date(task.getStartDate().getTime()));
        else
            statement.setDate(5, null);
        if (task.getFinishDate() != null)
            statement.setDate(6, new java.sql.Date(task.getFinishDate().getTime()));
        else
            statement.setDate(6, null);
        statement.setString(7, task.getStatusType().displayName());
        statement.setString(8, task.getProjectId());
        statement.setString(9, task.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    public Task findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = ? AND id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Task findOneById]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @Nullable Task result = null;
        if (resultSet.next()) result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Nullable
    public Task findOneByName(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = ? AND name = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Task findOneByName]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @Nullable Task result = null;
        if (resultSet.next()) result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String query = "DELETE FROM `my-task-manager_db`.app_task WHERE user_id = ? AND id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Task remove]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeAll(@NotNull final String userId) throws Exception {
        @NotNull final String query = "DELETE FROM `my-task-manager_db`.app_task WHERE user_id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Task removeAll]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByCreateDate(@NotNull final String userId) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = ? ORDER BY create_date ASC;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. List<Task> findAllSortedByCreateDate]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByStartDate(@NotNull final String userId) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = ? ORDER BY start_date ASC;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. List<Task> findAllSortedByStartDate]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByFinishDate(@NotNull final String userId) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = ? ORDER BY finish_date ASC;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. List<Task> findAllSortedByFinishDate]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByStatus(@NotNull final String userId) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = ? ORDER BY FIELD(status, 'PLANNED', 'INPROCESS', 'READY');";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. List<Task> findAllSortedByStatus]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public List<Task> findAllBySearch(@NotNull final String userId, @NotNull final String search) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = " +
                "AND (name LIKE \"%" + search + "%\" OR description LIKE \"%" + search + "%\");";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. List<Task> findAllBySearch]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    
    @Override
    public void removeTasksWithProjectId(@NotNull final String userId) throws Exception {
        @NotNull final String query = "DELETE FROM `my-task-manager_db`.app_task WHERE  user_id = ? AND project_id IS NOT NULL;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. removeTasksWithProjectId]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeProjectTasks(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final String query = "DELETE FROM `my-task-manager_db`.app_task WHERE user_id = ? AND project_id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. removeProjectTasks]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    public final List<Task> findTasksByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = ? AND project_id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. List<Task> findTasksByProjectId]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public final List<Task> findTasksWithoutProject(@NotNull final String userId) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = ? AND project_id IS NULL;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. List<Task> findTasksWithoutProject]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }
}
