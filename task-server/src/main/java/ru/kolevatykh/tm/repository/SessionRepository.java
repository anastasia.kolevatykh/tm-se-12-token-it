package ru.kolevatykh.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ISessionRepository;
import ru.kolevatykh.tm.constant.FieldConst;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    private Session fetch(@Nullable final ResultSet row) throws Exception {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString(FieldConst.ID));
        session.setSignature(row.getString(FieldConst.SIGNATURE));
        session.setTimestamp(row.getLong(FieldConst.TIMESTAMP));
        session.setRoleType(RoleType.valueOf(row.getString(FieldConst.ROLE_TYPE)));
        session.setUserId(row.getString(FieldConst.USER_ID));
        return session;
    }

    @Override
    public @NotNull List<Session> findAll() throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_session";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. List<Session> findAll]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Session> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        statement.close();
        return result;
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String id) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_session WHERE id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Session findOneById]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @Nullable Session result = null;
        if (resultSet.next()) result = fetch(resultSet);
        resultSet.close();
        statement.close();
        return result;
    }

    @Override
    public void persist(@NotNull final Session session) throws Exception {
        @NotNull final String query = "INSERT INTO `my-task-manager_db`.app_session VALUES (?, ?, ?, ?, ?);";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Session persist]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setString(2, session.getSignature());
        statement.setLong(3, session.getTimestamp());
        statement.setString(4, session.getUserId());
        statement.setString(5, session.getRoleType().displayName());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void merge(@NotNull final Session session) throws Exception {
        @NotNull final String query = "UPDATE INTO `my-task-manager_db`.app_session SET "
                + "signature = ?, timestamp = ?, user_id = ?, role_type = ? WHERE id = ?);";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Session merge]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setString(2, session.getSignature());
        statement.setLong(3, session.getTimestamp());
        statement.setString(4, session.getUserId());
        statement.setString(5, session.getRoleType().displayName());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void remove(@NotNull final String id) throws Exception {
        @NotNull final String query = "DELETE FROM `my-task-manager_db`.app_session WHERE id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Session remove]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String query = "DELETE FROM `my-task-manager_db`.app_session;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Session removeAll]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) throws Exception {
        @NotNull final String query = "DELETE FROM `my-task-manager_db`.app_session WHERE user_id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Project removeAll]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }
}
