package ru.kolevatykh.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.AbstractEntity;
import java.sql.Connection;
import java.util.*;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractRepository<T extends AbstractEntity> {

    @NotNull
    private Connection connection;

    AbstractRepository(@NotNull final Connection connection) {
        this.setConnection(connection);
    }
    @NotNull
    abstract public List<T> findAll() throws Exception;

    @Nullable
    abstract public T findOneById(@NotNull final String id) throws Exception;

    abstract void persist(@NotNull final T entity) throws Exception;

    abstract void merge(@NotNull final T entity) throws Exception;

    abstract void remove(@NotNull final String id) throws Exception;

    abstract void removeAll() throws Exception;
}
