package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IProjectRepository;
import ru.kolevatykh.tm.constant.FieldConst;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.enumerate.StatusType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

public final class ProjectRepository extends AbstractProjectTaskRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }
    
    @Nullable
    private Project fetch(@Nullable final ResultSet row) throws Exception {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString(FieldConst.ID));
        project.setUserId(row.getString(FieldConst.USER_ID));
        project.setName(row.getString(FieldConst.NAME));
        project.setDescription(row.getString(FieldConst.DESCRIPTION));
        project.setCreateDate(row.getDate(FieldConst.CREATE_DATE));
        project.setStartDate(row.getDate(FieldConst.START_DATE));
        project.setFinishDate(row.getDate(FieldConst.FINISH_DATE));
        project.setStatusType(StatusType.valueOf(row.getString(FieldConst.STATUS_TYPE)));
        return project;
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_project";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Project findAll]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }
    
    @NotNull
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = ?";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Project findAllByUserId]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    public void persist(@NotNull final Project project) throws Exception {
        @NotNull final String query = "INSERT INTO `my-task-manager_db`.app_project VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Project persist]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getUserId());
        statement.setString(3, project.getName());
        statement.setString(4, project.getDescription());
        statement.setDate(5, new java.sql.Date(project.getCreateDate().getTime()));
        if (project.getStartDate() != null)
            statement.setDate(6, new java.sql.Date(project.getStartDate().getTime()));
        else
            statement.setDate(6, null);
        if (project.getFinishDate() != null)
            statement.setDate(7, new java.sql.Date(project.getFinishDate().getTime()));
        else
            statement.setDate(7, null);
        statement.setString(8, project.getStatusType().displayName());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void merge(@NotNull final Project project) throws Exception {
        @NotNull final String query = "UPDATE `my-task-manager_db`.app_project SET name = ?, description = ?," +
                "create_date = ?, start_date = ?, finish_date = ?, status_type = ? WHERE id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Project merge]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, project.getName());
        statement.setString(2, project.getDescription());
        statement.setDate(3, new java.sql.Date(project.getCreateDate().getTime()));
        if (project.getStartDate() != null)
            statement.setDate(4, new java.sql.Date(project.getStartDate().getTime()));
        else
            statement.setDate(4, null);
        if (project.getFinishDate() != null)
            statement.setDate(5, new java.sql.Date(project.getFinishDate().getTime()));
        else
            statement.setDate(5, null);
        statement.setString(6, project.getStatusType().displayName());
        statement.setString(7, project.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    public Project findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = ? AND id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Project findOneById.]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @Nullable Project result = null;
        if (resultSet.next()) result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Nullable
    public Project findOneByName(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = ? AND name = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Project findOneByName]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @Nullable Project result = null;
        if (resultSet.next()) result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String query = "DELETE FROM `my-task-manager_db`.app_project WHERE user_id = ? AND id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Project remove]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeAll(@NotNull final String userId) throws Exception {
        @NotNull final String query = "DELETE FROM `my-task-manager_db`.app_project WHERE user_id = ?;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. Project removeAll]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByCreateDate(@NotNull final String userId) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = ? ORDER BY create_date ASC;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. List<Project> findAllSortedByCreateDate]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByStartDate(@NotNull final String userId) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = ? ORDER BY start_date ASC;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. List<Project> findAllSortedByStartDate]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByFinishDate(@NotNull final String userId) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = ? ORDER BY finish_date ASC;";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. List<Project> findAllSortedByFinishDate]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByStatus(@NotNull final String userId) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = ? ORDER BY FIELD(status, 'PLANNED', 'INPROCESS', 'READY');";
        if (getConnection() == null) throw new Exception("[The connection to database is not set.  List<Project> findAllSortedByStatus]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public List<Project> findAllBySearch(@NotNull final String userId, @NotNull final String search) throws Exception {
        @NotNull final String query = "SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = " +
                "AND (name LIKE \"%" + search + "%\" OR description LIKE \"%" + search + "%\");";
        if (getConnection() == null) throw new Exception("[The connection to database is not set. List<Project> findAllBySearch]");
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }
}
