package ru.kolevatykh.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.AbstractProjectTaskEntity;
import java.sql.Connection;
import java.util.*;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractProjectTaskRepository<T extends AbstractProjectTaskEntity> {

    @NotNull
    private Connection connection;

    AbstractProjectTaskRepository(@NotNull final Connection connection) {
        this.setConnection(connection);
    }

    @NotNull
    abstract public List<T> findAll() throws Exception;

    @NotNull
    abstract public List<T> findAllByUserId(@NotNull final String userId) throws Exception;

    @Nullable
    abstract public T findOneById(@NotNull final String userId, @NotNull final String id) throws Exception;

    @Nullable
    abstract public T findOneByName(@NotNull final String userId, @NotNull final String name) throws Exception;

    abstract public void persist(@NotNull final T entity) throws Exception;

    abstract public void merge(@NotNull final T entity) throws Exception;

    abstract public void remove(@NotNull final String userId, @NotNull final String id) throws Exception;

    abstract public void removeAll(@NotNull final String userId) throws Exception;

    @NotNull
    abstract public List<T> findAllSortedByCreateDate(@NotNull final String userId) throws Exception;

    @NotNull
    abstract public List<T> findAllSortedByStartDate(@NotNull final String userId) throws Exception;

    @NotNull
    abstract public List<T> findAllSortedByFinishDate(@NotNull final String userId) throws Exception;

    @NotNull
    abstract public List<T> findAllSortedByStatus(@NotNull final String userId) throws Exception;

    @NotNull
    abstract public List<T> findAllBySearch(@NotNull final String userId, @NotNull final String search) throws Exception;
}
