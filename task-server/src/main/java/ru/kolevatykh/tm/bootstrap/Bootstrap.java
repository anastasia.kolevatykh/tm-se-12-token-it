package ru.kolevatykh.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import ru.kolevatykh.tm.api.*;
import ru.kolevatykh.tm.endpoint.*;
import ru.kolevatykh.tm.repository.SessionRepository;
import ru.kolevatykh.tm.service.*;

import javax.xml.ws.Endpoint;

@Getter
@NoArgsConstructor(force = true)
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final IUserService userService = new UserService();

    @NotNull
    private final IProjectService projectService = new ProjectService();

    @NotNull
    private final ITaskService taskService = new TaskService();

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, userService);

    @NotNull
    private final TokenService tokenService = new TokenService(sessionService);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ITokenEndpoint tokenEndpoint = new TokenEndpoint(this);

    public void init() throws Exception {
        Endpoint.publish(TaskEndpoint.URL, taskEndpoint);
        Endpoint.publish(ProjectEndpoint.URL, projectEndpoint);
        Endpoint.publish(UserEndpoint.URL, userEndpoint);
        Endpoint.publish(DomainEndpoint.URL, domainEndpoint);
        Endpoint.publish(TokenEndpoint.URL, tokenEndpoint);

        System.out.println(TaskEndpoint.URL);
        System.out.println(ProjectEndpoint.URL);
        System.out.println(UserEndpoint.URL);
        System.out.println(DomainEndpoint.URL);
        System.out.println(TokenEndpoint.URL);

        getDomainService().loadJacksonJson();
    }
}
