package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ISessionService;
import ru.kolevatykh.tm.api.IUserService;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.api.ISessionRepository;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.repository.SessionRepository;
import ru.kolevatykh.tm.util.ConfigUtil;
import ru.kolevatykh.tm.util.SignatureUtil;

import java.sql.Connection;
import java.sql.Savepoint;
import java.util.List;

import static ru.kolevatykh.tm.util.DatabaseConnectionUtil.getConnection;

@Setter
@Getter
@NoArgsConstructor
public final class SessionService extends AbstractService<Session> implements ISessionService {

    @Nullable
    private IUserService userService;

    @Nullable
    private ISessionRepository sessionRepository;

    public SessionService(@NotNull final ISessionRepository sessionRepository,
                          @NotNull final IUserService userService) {
        this.setSessionRepository(sessionRepository);
        this.setUserService(userService);
    }

    @Override
    @Nullable
    public User getUser(@Nullable final Session session) throws Exception {
        if (userService == null || sessionRepository == null || session == null) return null;
        @Nullable final User user = userService.findOneById(session.getUserId());
        return null;
    }

    @Override
    @Nullable
    public Session openAuth(@Nullable final String login, @Nullable final String password) throws Exception {
        if (userService == null ||login == null || login.isEmpty()
                || password == null || password.isEmpty()) return null;
        @Nullable User user = userService.findOneByLogin(login);
        if (user == null) throw new Exception("There's no such user \'" + login + "\'.");
        if (!user.getPasswordHash().equals(password)) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRoleType(user.getRoleType());
        session.setSignature(SignatureUtil.sign(session, ConfigUtil.getSalt(), 13));
        persist(session);
        return session;
    }

    @Override
    @Nullable
    public Session openReg(@Nullable final String login, @Nullable final String password) throws Exception {
        if (userService == null ||login == null || login.isEmpty()
                || password == null || password.isEmpty()) return null;
        @Nullable User user = new User(login, password, RoleType.USER);
        userService.persist(user);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRoleType(user.getRoleType());
        session.setSignature(SignatureUtil.sign(session, ConfigUtil.getSalt(), 13));
        persist(session);
        return session;
    }

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (sessionRepository == null || session == null) return;
        @NotNull final long timeOut = 2 * 60 * 1000L;
        if (System.currentTimeMillis() - session.getTimestamp() > timeOut) {
            remove(session.getId());
            throw new Exception("[The session is timed out.]");
        }
        @NotNull final String signature = Session.generateSignature(session);
        if (!signature.equals(session.getSignature()))
            throw new Exception("[Wrong session signature.]");
        if (findOneById(session.getId()) == null)
            throw new Exception("[The session does not exist.]");
    }

    @Override
    public @Nullable List<Session> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Session> list = new SessionRepository(connection).findAll();
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @Nullable final Session session = new SessionRepository(connection).findOneById(id);
        if (session == null) return null;
        return session;
    }

    @Override
    void persist(@Nullable final Session session) throws Exception {
        if (session == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try {
            new SessionRepository(connection).persist(session);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Override
    void merge(@NotNull final Session session) throws Exception {
        if (session == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try {
            new SessionRepository(connection).merge(session);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try {
            new SessionRepository(connection).remove(id);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try {
            new SessionRepository(connection).removeAll();
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try {
            new SessionRepository(connection).removeAllByUserId(userId);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }
}
