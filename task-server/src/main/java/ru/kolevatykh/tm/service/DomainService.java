package ru.kolevatykh.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IDomainService;
import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.entity.Domain;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;

import javax.xml.bind.*;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
public final class DomainService implements IDomainService {

    @Nullable
    private ServiceLocator serviceLocator;

    public DomainService(@NotNull final ServiceLocator serviceLocator) {
        this.setServiceLocator(serviceLocator);
    }

    @Nullable
    private Domain findAll() throws Exception {
        if (serviceLocator == null) return null;
        @NotNull final Domain domain = new Domain();
        domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        return domain;
    }

    private void loadAll(@NotNull final Domain domain) throws Exception {
        if (serviceLocator == null) return;
        if (domain.getUsers() != null) {
            for (@NotNull final User user : domain.getUsers()) {
                serviceLocator.getUserService().persist(user);
            }
        }
        if (domain.getProjects() != null) {
            for (@NotNull final Project project : domain.getProjects()) {
                serviceLocator.getProjectService().persist(project);
            }
        }
        if (domain.getTasks() != null) {
            for (@NotNull final Task task : domain.getTasks()) {
                serviceLocator.getTaskService().persist(task);
            }
        }
    }

    @Override
    public void serialize() throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Domain domain = findAll();
        @NotNull final ObjectOutputStream objectOutputStream =
                new ObjectOutputStream(new FileOutputStream("task-server/src/main/resources/file/domain.out"));
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
    }

    @Override
    public void deserialize() throws Exception {
        @NotNull final ObjectInputStream objectInputStream =
                new ObjectInputStream(new FileInputStream("task-server/src/main/resources/file/domain.out"));
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        loadAll(domain);
    }

    @Override
    public void saveJacksonJson() throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Domain domain = findAll();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper
                .writerWithDefaultPrettyPrinter()
                .writeValue(new File("task-server/src/main/resources/file/jacksonDomain.json"), domain);
    }

    @Override
    public void loadJacksonJson() throws Exception {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        InputStream is = DomainService.class.getResourceAsStream("/file/jacksonDomain.json");
        @NotNull final Domain domain = objectMapper.readValue(is, Domain.class);
        loadAll(domain);
    }

    @Override
    public void saveJacksonXml() throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Domain domain = findAll();
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper
                .writerWithDefaultPrettyPrinter()
                .writeValue(new File("task-server/src/main/resources/file/jacksonDomain.xml"), domain);
    }

    @Override
    public void loadJacksonXml() throws Exception {
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        InputStream is = DomainService.class.getResourceAsStream("/file/jacksonDomain.xml");
        @NotNull final Domain domain = xmlMapper.readValue(is, Domain.class);
        loadAll(domain);
    }

    @Override
    public void marshalJaxbJson() throws Exception {
        @Nullable final Domain domain = findAll();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, properties);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, false);
        marshaller.marshal(domain, new File("task-server/src/main/resources/file/jaxbDomain.json"));
    }

    @Override
    public void unmarshalJaxbJson() throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, properties);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty("eclipselink.json.include-root", false);
        InputStream is = DomainService.class.getResourceAsStream("/file/jaxbDomain.json");
        @NotNull final StreamSource source = new StreamSource(is);
        @NotNull final JAXBElement<Domain> jaxbElement = unmarshaller.unmarshal(source, Domain.class);
        @NotNull final Domain domain = jaxbElement.getValue();
        loadAll(domain);
    }

    @Override
    public void marshalJaxbXml() throws Exception {
        @Nullable final Domain domain = findAll();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, properties);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, new File("task-server/src/main/resources/file/jaxbDomain.xml"));
    }

    @Override
    public void unmarshalJaxbXml() throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, properties);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        InputStream is = DomainService.class.getResourceAsStream("/file/jaxbDomain.xml");
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(is);
        loadAll(domain);
    }
}
