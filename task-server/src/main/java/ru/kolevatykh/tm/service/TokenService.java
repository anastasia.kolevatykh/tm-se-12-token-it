package ru.kolevatykh.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ISessionService;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.Token;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.util.AESUtil;
import ru.kolevatykh.tm.util.ConfigUtil;
import ru.kolevatykh.tm.util.SignatureUtil;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public final class TokenService {

    @Nullable
    private ISessionService sessionService;

    public TokenService(@NotNull final ISessionService sessionService) {
        this.setSessionService(sessionService);
    }

    public void closeAllTokenSession() throws Exception {
        if (sessionService == null) return;
        sessionService.removeAll();
    }

    public void closeTokenSession(@NotNull final Token token) throws Exception {
        if (sessionService == null) return;
        if (token.getSession() != null)
            sessionService.remove(token.getSession().getId());
    }

    @Nullable
    public List<Session> getListTokenSession() throws Exception {
        if (sessionService == null) return null;
        return sessionService.findAll();
    }

    @Nullable
    public User getUser(@Nullable final Token token) throws Exception {
        if (sessionService == null || token == null) return null;
        @Nullable final User user = sessionService.getUser(token.getSession());
        return user;
    }

    @Nullable
    public String openAuth(@Nullable final String login, @Nullable final String password) throws Exception {
        if (sessionService == null || login == null || login.isEmpty()
                || password == null || password.isEmpty()) return null;
        @Nullable final Session session = sessionService.openAuth(login, password);
        if (session == null) return null;
        @NotNull final Token token = new Token();
        token.setSession(session);
        token.setSignature(SignatureUtil.sign(token, ConfigUtil.getSalt(), 13));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String jsonToken = objectMapper.writeValueAsString(token);
        @NotNull final String encryptedToken = AESUtil.encrypt(jsonToken, ConfigUtil.getKey());
        return encryptedToken;
    }

    @Nullable
    public String openReg(@Nullable final String login, @Nullable final String password) throws Exception {
        if (sessionService == null || login == null || login.isEmpty()
                || password == null || password.isEmpty()) return null;
        @Nullable final Session session = sessionService.openReg(login, password);
        if (session == null) return null;
        @NotNull final Token token = new Token();
        token.setSession(session);
        token.setSignature(SignatureUtil.sign(token, ConfigUtil.getSalt(), 13));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String jsonToken = objectMapper.writeValueAsString(token);
        @NotNull final String encryptedToken = AESUtil.encrypt(jsonToken, ConfigUtil.getKey());
        return encryptedToken;
    }

    public Token validate(@Nullable final String tokenString) throws Exception {
        if (sessionService == null || tokenString == null) return null;
        @NotNull final String decryptedToken = AESUtil.decrypt(tokenString, ConfigUtil.getKey());
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Token token = objectMapper.readValue(decryptedToken, Token.class);
        sessionService.validate(token.getSession());
        @NotNull final String signature = Token.generateSignature(token);
        if (!signature.equals(token.getSignature()))
            throw new Exception("[Wrong token signature.]");
        return token;
    }
}
