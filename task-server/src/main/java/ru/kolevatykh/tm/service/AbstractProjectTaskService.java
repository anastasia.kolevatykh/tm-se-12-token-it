package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.AbstractProjectTaskEntity;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractProjectTaskService<T extends AbstractProjectTaskEntity> {

    @Nullable
    abstract public List<T> findAll() throws Exception;

    @Nullable
    abstract public List<T> findAllByUserId(@Nullable final String userId) throws Exception;

    @Nullable
    abstract public T findOneById(@Nullable final String userId, @Nullable final String id) throws Exception;

    @Nullable
    abstract public T findOneByName(@Nullable final String userId, @Nullable final String name) throws Exception;

    abstract public void persist(@Nullable final T entity) throws Exception;

    abstract public void remove(@Nullable final String userId, @Nullable final String id) throws Exception;

    abstract public void removeAll(@Nullable final String userId) throws Exception;

    @Nullable
    abstract public List<T> findAllSortedByCreateDate(@Nullable final String userId) throws Exception;

    @Nullable
    abstract public List<T> findAllSortedByStartDate(@Nullable final String userId) throws Exception;

    @Nullable
    abstract public List<T> findAllSortedByFinishDate(@Nullable final String userId) throws Exception;

    @Nullable
    abstract public List<T> findAllSortedByStatus(@Nullable final String userId) throws Exception;

    @Nullable
    abstract public List<T> findAllBySearch(@Nullable final String userId, @Nullable final String search) throws Exception;
}
