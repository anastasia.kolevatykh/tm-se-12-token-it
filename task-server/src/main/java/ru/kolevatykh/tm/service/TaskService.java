package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ITaskService;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.Savepoint;
import java.util.List;

import static ru.kolevatykh.tm.util.DatabaseConnectionUtil.getConnection;

@Getter
@Setter
public final class TaskService extends AbstractProjectTaskService<Task> implements ITaskService {

    @Nullable
    @Override
    public final List<Task> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Task> list = new TaskRepository(connection).findAll();
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Task> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Task> list = new TaskRepository(connection).findAllByUserId(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final Task findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).findOneById(userId, id);
    }

    @Nullable
    @Override
    public final Task findOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (name == null || name.isEmpty() || userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).findOneByName(userId, name);
    }

    @Override
    public void persist(@Nullable final Task task) throws Exception {
        if (task == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try{
            new TaskRepository(connection).persist(task);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Override
    public void merge(@Nullable final Task task) throws Exception {
        if (task == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try{
            new TaskRepository(connection).merge(task);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try{
            new TaskRepository(connection).remove(userId, id);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try{
            new TaskRepository(connection).removeAll(userId);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public final List<Task> findAllSortedByCreateDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Task> list = new TaskRepository(connection).findAllSortedByCreateDate(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Task> findAllSortedByStartDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Task> list = new TaskRepository(connection).findAllSortedByStartDate(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Task> findAllSortedByFinishDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Task> list = new TaskRepository(connection).findAllSortedByFinishDate(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Task> findAllSortedByStatus(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Task> list = new TaskRepository(connection).findAllSortedByStatus(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Task> findAllBySearch(@Nullable final String userId, @Nullable final String search) throws Exception {
        if (userId == null || userId.isEmpty() || search == null || search.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Task> list = new TaskRepository(connection).findAllBySearch(userId, search);
        if (list.isEmpty()) return null;
        return list;
    }

    @Override
    public void removeTasksWithProjectId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try{
            new TaskRepository(connection).removeTasksWithProjectId(userId);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeProjectTasks(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty() || userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try{
            new TaskRepository(connection).removeProjectTasks(userId, projectId);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public final List<Task> findTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty() || projectId == null || projectId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Task> list = new TaskRepository(connection).findTasksByProjectId(userId, projectId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Task> findTasksWithoutProject(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Task> list = new TaskRepository(connection).findTasksWithoutProject(userId);
        if (list.isEmpty()) return null;
        return list;
    }
}
