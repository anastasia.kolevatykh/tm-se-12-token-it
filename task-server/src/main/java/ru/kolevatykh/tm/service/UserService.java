package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IUserService;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.repository.UserRepository;

import java.sql.Connection;
import java.sql.Savepoint;
import java.util.List;

import static ru.kolevatykh.tm.util.DatabaseConnectionUtil.getConnection;

@Setter
@Getter
@NoArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @Override
    public @Nullable List<User> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<User> list = new UserRepository(connection).findAll();
        if (list.isEmpty()) return null;
        return list;
    }

    @Override
    public @Nullable User findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @Nullable final User user = new UserRepository(connection).findOneById(id);
        if (user == null) return null;
        return user;
    }

    @Nullable
    @Override
    public final User findOneByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @Nullable final User user = new UserRepository(connection).findOneByLogin(login);
        if (user == null) return null;
        return user;
    }

    public void persist(@Nullable final User user) throws Exception {
        if (user == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try {
            new UserRepository(connection).persist(user);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Override
    public void merge(@Nullable User user) throws Exception {
        if (user == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try {
            new UserRepository(connection).merge(user);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try {
            new UserRepository(connection).remove(id);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try {
            new UserRepository(connection).removeAll();
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }
}
