package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IProjectService;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.sql.Savepoint;
import java.util.List;

import static ru.kolevatykh.tm.util.DatabaseConnectionUtil.getConnection;

@Setter
@Getter
@NoArgsConstructor
public final class ProjectService extends AbstractProjectTaskService<Project> implements IProjectService {

    @Nullable
    @Override
    public final List<Project> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Project> list = new ProjectRepository(connection).findAll();
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override

    public final List<Project> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Project> list = new ProjectRepository(connection).findAllByUserId(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final Project findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).findOneById(userId, id);
    }

    @Nullable
    @Override
    public final Project findOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (name == null || name.isEmpty() || userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).findOneByName(userId, name);
    }

    @Override
    public void persist(@Nullable final Project project) throws Exception {
        if (project == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try {
            new ProjectRepository(connection).persist(project);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Override
    public void merge(@Nullable final Project project) throws Exception {
        if (project == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try {
            new ProjectRepository(connection).merge(project);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try {
            new ProjectRepository(connection).remove(userId, id);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        Savepoint save = connection.setSavepoint();
        try {
            new ProjectRepository(connection).removeAll(userId);
            connection.commit();
        } catch (Exception e) {
            connection.rollback(save);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public final List<Project> findAllSortedByCreateDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Project> list = new ProjectRepository(connection).findAllSortedByCreateDate(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Project> findAllSortedByStartDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Project> list = new ProjectRepository(connection).findAllSortedByStartDate(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Project> findAllSortedByFinishDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Project> list = new ProjectRepository(connection).findAllSortedByFinishDate(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Project> findAllSortedByStatus(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Project> list = new ProjectRepository(connection).findAllSortedByStatus(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Project> findAllBySearch(@Nullable final String userId, @Nullable final String search) throws Exception {
        if (userId == null || userId.isEmpty() || search == null || search.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final List<Project> list = new ProjectRepository(connection).findAllBySearch(userId, search);
        if (list.isEmpty()) return null;
        return list;
    }
}
