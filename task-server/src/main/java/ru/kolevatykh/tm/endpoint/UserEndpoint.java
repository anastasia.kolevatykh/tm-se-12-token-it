package ru.kolevatykh.tm.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.Token;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Setter
@Getter
@WebService(endpointInterface = "ru.kolevatykh.tm.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint() {
    }

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public final List<User> findAllUsers(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        if (!RoleType.ADMIN.equals(session.getRoleType()))
            throw new Exception("[Command is not allowed. You need Admin role for this command.]");
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @Nullable
    @WebMethod
    public final User findUserById(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getUserService().findOneById(session.getUserId());
    }

    @Override
    @WebMethod
    public void mergeUser(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "loginNew") @NotNull final String loginNew,
            @WebParam(name = "password") @NotNull final String password,
            @WebParam(name = "role") @Nullable final String role,
            @WebParam(name = "isAuth") @Nullable final Boolean isAuth
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        @Nullable final Session session = token.getSession();
        if (session == null) return;
        @Nullable final User user = serviceLocator.getUserService().findOneByLogin(login);
        if (user == null) throw new Exception("[The login is not found.]");
        user.setLogin(loginNew);
        user.setPasswordHash(password);
        user.setRoleType(session.getRoleType());
        serviceLocator.getUserService().merge(user);
    }

    @Override
    @WebMethod
    public void removeUser(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        @Nullable final Session session = token.getSession();
        if (session == null) return;
        @Nullable final String userId = session.getUserId();
        serviceLocator.getSessionService().removeAllByUserId(session.getUserId());
        serviceLocator.getUserService().remove(userId);
    }

    @Override
    @WebMethod
    public void removeAllUsers(
            @WebParam(name = "token") @Nullable final String tokenString

    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        serviceLocator.getUserService().removeAll();
    }
}
