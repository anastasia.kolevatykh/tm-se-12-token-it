package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.Token;
import ru.kolevatykh.tm.enumerate.StatusType;
import ru.kolevatykh.tm.util.DateFormatterUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kolevatykh.tm.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Project> findAllProjectsByUserId(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getProjectService().findAllByUserId(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final Project findProjectById(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getProjectService().findOneById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public final Project findProjectByName(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getProjectService().findOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void persistProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        @Nullable final Session session = token.getSession();
        if (session == null) return;
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(session.getUserId());
        project.setStartDate(DateFormatterUtil.parseDate(startDate));
        project.setFinishDate(DateFormatterUtil.parseDate(finishDate));
        serviceLocator.getProjectService().persist(project);
    }

    @Override
    @WebMethod
    public void mergeProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        @Nullable final Session session = token.getSession();
        if (session == null) return;
        @Nullable final Project project = serviceLocator.getProjectService().findOneById(session.getUserId(), id);
        if (project == null) throw new Exception("[The project does not exist.]");
        project.setName(name);
        project.setDescription(description);
        project.setUserId(session.getUserId());
        project.setStartDate(DateFormatterUtil.parseDate(startDate));
        project.setFinishDate(DateFormatterUtil.parseDate(finishDate));
        serviceLocator.getProjectService().merge(project);
    }

    @Override
    @WebMethod
    public void mergeProjectStatus(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "status") @Nullable final String status
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        @Nullable final Session session = token.getSession();
        if (session == null) return;
        @Nullable final Project project = serviceLocator.getProjectService().findOneById(session.getUserId(), id);
        if (project == null) throw new Exception("[The project does not exist.]");
        project.setStatusType(StatusType.valueOf(status));
        serviceLocator.getProjectService().merge(project);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        @Nullable final Session session = token.getSession();
        if (session == null) return;
        serviceLocator.getProjectService().remove(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeAllProjects(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        @Nullable final Session session = token.getSession();
        if (session == null) return;
        serviceLocator.getProjectService().removeAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Project> findProjectsSortedByCreateDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getProjectService().findAllSortedByCreateDate(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Project> findProjectsSortedByStartDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getProjectService().findAllSortedByStartDate(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Project> findProjectsSortedByFinishDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getProjectService().findAllSortedByFinishDate(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Project> findProjectsSortedByStatus(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getProjectService().findAllSortedByStatus(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Project> findProjectsBySearch(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "search") @Nullable final String search
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getProjectService().findAllBySearch(session.getUserId(), search);
    }
}
