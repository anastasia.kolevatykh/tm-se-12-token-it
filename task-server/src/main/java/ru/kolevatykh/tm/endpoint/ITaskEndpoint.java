package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @NotNull String URL = "http://0.0.0.0:1234/TaskEndpoint?wsdl";

    @Nullable
    @WebMethod
    List<Task> findAllTasksByUserId(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    Task findTaskById(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @Nullable
    @WebMethod
    Task findTaskByName(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "name") @Nullable String name
    ) throws Exception;

    @WebMethod
    void persistTask(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception;

    @WebMethod
    void mergeTask(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception;

    @WebMethod
    void mergeTaskStatus(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String status
    ) throws Exception;

    @WebMethod
    void removeTask(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @WebMethod
    void removeAllTasks(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTasksSortedByCteateDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTasksSortedByStartDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTasksSortedByFinishDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTasksSortedByStatus(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTasksBySearch(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "search") @Nullable String search
    ) throws Exception;

    @WebMethod
    void removeTasksWithProjectId(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @WebMethod
    void removeProjectTasks(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "projectId") @Nullable String projectId
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTasksByProjectId(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "projectId") @Nullable String projectId
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTasksWithoutProject(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @WebMethod
    void assignToProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "projectId") @Nullable String projectId
    ) throws Exception;
}
