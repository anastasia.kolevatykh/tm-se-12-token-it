package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @NotNull String URL = "http://0.0.0.0:1234/UserEndpoint?wsdl";

    @Nullable
    @WebMethod
    List<User> findAllUsers(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    User findUserById(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @WebMethod
    void mergeUser(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "loginNew") @NotNull String loginNew,
            @WebParam(name = "password") @NotNull String password,
            @WebParam(name = "role") @Nullable String role,
            @WebParam(name = "isAuth") @Nullable Boolean isAuth
    ) throws Exception;

    @WebMethod
    void removeUser(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @WebMethod
    void removeAllUsers(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

//    @Nullable
//    @WebMethod
//    User findUserByLogin(
//            @WebParam(name = "login") @Nullable final String login
//    ) throws Exception;
}
