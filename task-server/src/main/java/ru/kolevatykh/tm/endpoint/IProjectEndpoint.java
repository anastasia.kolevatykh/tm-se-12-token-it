package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @NotNull String URL = "http://0.0.0.0:1234/ProjectEndpoint?wsdl";

    @Nullable
    @WebMethod
    List<Project> findAllProjectsByUserId(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    Project findProjectById(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @Nullable
    @WebMethod
    Project findProjectByName(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "name") @Nullable String name
    ) throws Exception;

    @WebMethod
    void persistProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception;

    @WebMethod
    void mergeProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception;

    @WebMethod
    void mergeProjectStatus(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String status
    ) throws Exception;

    @WebMethod
    void removeProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @WebMethod
    void removeAllProjects(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    public List<Project> findProjectsSortedByCreateDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Project> findProjectsSortedByStartDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Project> findProjectsSortedByFinishDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Project> findProjectsSortedByStatus(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Project> findProjectsBySearch(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "search") @Nullable String search
    ) throws Exception;
}
