package ru.kolevatykh.tm;

import org.apache.log4j.PropertyConfigurator;
import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.bootstrap.Bootstrap;
import java.io.InputStream;

public final class ApplicationServer {
    public static void main(String[] args) throws Exception {
        InputStream is = ApplicationServer.class.getResourceAsStream("/META-INF/log4j.properties");
        PropertyConfigurator.configure(is);
        is.close();
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
