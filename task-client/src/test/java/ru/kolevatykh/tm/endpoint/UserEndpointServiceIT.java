package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.kolevatykh.tm.util.PasswordHashUtil;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserEndpointServiceIT {

    @Nullable
    private ITokenEndpoint tokenEndpoint = null;

    @Nullable
    private IUserEndpoint userEndpoint = null;

    @Nullable
    private String token = null;

    @BeforeEach
    void setUp() {
        try {
            tokenEndpoint = new TokenEndpointService(new URL("http://0.0.0.0:1234/TokenEndpoint?wsdl")).getTokenEndpointPort();
            userEndpoint = new UserEndpointService(new URL("http://0.0.0.0:1234/UserEndpoint?wsdl")).getUserEndpointPort();
        } catch (MalformedURLException e) {
            assertNotNull(null);
        }
        @NotNull final String login = "admin";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("pass1");
        try {
            assertNotNull(tokenEndpoint);
            token = tokenEndpoint.openTokenSessionAuth(login, pass);
        } catch (Exception_Exception e) {
            assertNotNull(null);
        }
    }

    @AfterEach
    void tearDown() {
        try {
            assertNotNull(tokenEndpoint);
            tokenEndpoint.closeAllTokenSession(token);
        } catch (Exception_Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void getUserAthorizationTokenTest() {
        @NotNull final String login = "admin";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("pass1");
        try {
            assertNotNull(tokenEndpoint);
            token = tokenEndpoint.openTokenSessionAuth(login, pass);
        } catch (Exception_Exception e) {
            assertNotNull(null);
        }
        assertNotNull(token);
    }

    @Test
    void getUserListTest() {
        @NotNull List<User> userList = new ArrayList<>();
        try {
            assertNotNull(userEndpoint);
            userList = userEndpoint.findAllUsers(token);
        } catch (Exception_Exception e) {
            assertFalse(userList.isEmpty());
        }
    }
}