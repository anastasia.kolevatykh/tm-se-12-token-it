package ru.kolevatykh.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-03-28T22:59:37.250+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.kolevatykh.ru/", name = "ITokenEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ITokenEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/getUserByTokenRequest", output = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/getUserByTokenResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/getUserByToken/Fault/Exception")})
    @RequestWrapper(localName = "getUserByToken", targetNamespace = "http://endpoint.tm.kolevatykh.ru/", className = "ru.kolevatykh.tm.endpoint.GetUserByToken")
    @ResponseWrapper(localName = "getUserByTokenResponse", targetNamespace = "http://endpoint.tm.kolevatykh.ru/", className = "ru.kolevatykh.tm.endpoint.GetUserByTokenResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.kolevatykh.tm.endpoint.User getUserByToken(
        @WebParam(name = "token", targetNamespace = "")
        java.lang.String token
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/openTokenSessionAuthRequest", output = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/openTokenSessionAuthResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/openTokenSessionAuth/Fault/Exception")})
    @RequestWrapper(localName = "openTokenSessionAuth", targetNamespace = "http://endpoint.tm.kolevatykh.ru/", className = "ru.kolevatykh.tm.endpoint.OpenTokenSessionAuth")
    @ResponseWrapper(localName = "openTokenSessionAuthResponse", targetNamespace = "http://endpoint.tm.kolevatykh.ru/", className = "ru.kolevatykh.tm.endpoint.OpenTokenSessionAuthResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.lang.String openTokenSessionAuth(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/closeAllTokenSessionRequest", output = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/closeAllTokenSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/closeAllTokenSession/Fault/Exception")})
    @RequestWrapper(localName = "closeAllTokenSession", targetNamespace = "http://endpoint.tm.kolevatykh.ru/", className = "ru.kolevatykh.tm.endpoint.CloseAllTokenSession")
    @ResponseWrapper(localName = "closeAllTokenSessionResponse", targetNamespace = "http://endpoint.tm.kolevatykh.ru/", className = "ru.kolevatykh.tm.endpoint.CloseAllTokenSessionResponse")
    public void closeAllTokenSession(
        @WebParam(name = "token", targetNamespace = "")
        java.lang.String token
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/closeTokenSessionRequest", output = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/closeTokenSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/closeTokenSession/Fault/Exception")})
    @RequestWrapper(localName = "closeTokenSession", targetNamespace = "http://endpoint.tm.kolevatykh.ru/", className = "ru.kolevatykh.tm.endpoint.CloseTokenSession")
    @ResponseWrapper(localName = "closeTokenSessionResponse", targetNamespace = "http://endpoint.tm.kolevatykh.ru/", className = "ru.kolevatykh.tm.endpoint.CloseTokenSessionResponse")
    public void closeTokenSession(
        @WebParam(name = "token", targetNamespace = "")
        java.lang.String token
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/getListTokenSessionRequest", output = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/getListTokenSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/getListTokenSession/Fault/Exception")})
    @RequestWrapper(localName = "getListTokenSession", targetNamespace = "http://endpoint.tm.kolevatykh.ru/", className = "ru.kolevatykh.tm.endpoint.GetListTokenSession")
    @ResponseWrapper(localName = "getListTokenSessionResponse", targetNamespace = "http://endpoint.tm.kolevatykh.ru/", className = "ru.kolevatykh.tm.endpoint.GetListTokenSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.kolevatykh.tm.endpoint.Session> getListTokenSession(
        @WebParam(name = "token", targetNamespace = "")
        java.lang.String token
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/openTokenSessionRegRequest", output = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/openTokenSessionRegResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.kolevatykh.ru/ITokenEndpoint/openTokenSessionReg/Fault/Exception")})
    @RequestWrapper(localName = "openTokenSessionReg", targetNamespace = "http://endpoint.tm.kolevatykh.ru/", className = "ru.kolevatykh.tm.endpoint.OpenTokenSessionReg")
    @ResponseWrapper(localName = "openTokenSessionRegResponse", targetNamespace = "http://endpoint.tm.kolevatykh.ru/", className = "ru.kolevatykh.tm.endpoint.OpenTokenSessionRegResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.lang.String openTokenSessionReg(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    ) throws Exception_Exception;
}
