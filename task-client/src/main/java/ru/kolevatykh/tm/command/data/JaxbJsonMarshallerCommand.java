package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;

public final class JaxbJsonMarshallerCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "json-marshaller";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "jm";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tSave to json file JAX-B.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");
        System.out.println("[JSON MARSHALLER]");
        serviceLocator.getDomainEndpoint().marshalJaxbJson(token);
        System.out.println("[Saving is successful!]");
    }
}
