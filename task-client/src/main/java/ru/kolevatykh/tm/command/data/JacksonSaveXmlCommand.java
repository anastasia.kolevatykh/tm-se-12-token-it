package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;

public final class JacksonSaveXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "jackson-xml-save";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "xs";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tSave to xml file Jackson.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");
        System.out.println("[JACKSON XML SAVE]");
        serviceLocator.getDomainEndpoint().saveJacksonXml(token);
        System.out.println("[Saving is successful!]");
    }
}
