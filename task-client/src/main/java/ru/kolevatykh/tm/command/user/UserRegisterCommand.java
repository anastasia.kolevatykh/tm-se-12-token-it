package ru.kolevatykh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import ru.kolevatykh.tm.util.PasswordHashUtil;

public final class UserRegisterCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-register";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ur";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tRegister new user.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String currentToken = serviceLocator.getToken();
        if (currentToken != null) {
            throw new Exception("[You are authorized under login: "
                    + serviceLocator.getUserEndpoint().findUserById(currentToken).getLogin()
                    + ". Please LOGOUT first, in order to authorize under OTHER account.]");
        }

        System.out.println("[USER REGISTRATION]\nEnter your login: ");
        @NotNull final String login = ConsoleInputUtil.getConsoleInput();
        if (login.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        System.out.println("Enter your password: ");
        @NotNull final String password = ConsoleInputUtil.getConsoleInput();
        if (password.isEmpty()) {
            throw new Exception("[The password can't be empty.]");
        }

        @NotNull final String pass = PasswordHashUtil.getPasswordHash(password);
        @NotNull final String token = serviceLocator.getTokenEndpoint().openTokenSessionReg(login, pass);
        serviceLocator.setToken(token);
        System.out.println("[OK]");
    }
}
