package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.*;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import java.lang.Exception;

public final class TaskUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tu";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tUpdate selected task.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");

        System.out.println("[" + getName().toUpperCase() + "]");
        System.out.println("Enter task id: ");
        @NotNull final String taskId = ConsoleInputUtil.getConsoleInput();
        if (taskId.isEmpty()) {
            throw new Exception("[The id can't be empty.]");
        }

        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskById(token, taskId);
        if (task == null) {
            throw new Exception("[The task '" + taskId + "' does not exist!]");
        }

        System.out.println("Enter project id: ");
        @Nullable String projectId = ConsoleInputUtil.getConsoleInput();
        if (projectId.isEmpty() || projectId.equals("null")) {
            projectId = null;;
        } else {
            @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectById(token, projectId);
            if (project == null) {
                throw new Exception("[The project '" + projectId + "' does not exist!]");
            }
        }

        System.out.println("Enter new task name: ");
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();
        if (name.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        System.out.println("Enter new task description: ");
        @NotNull final String description = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter new task start date: ");
        @NotNull final String startDate = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter new task end date: ");
        @NotNull final String finishDate = ConsoleInputUtil.getConsoleInput();

        serviceLocator.getTaskEndpoint().mergeTask(token, projectId, taskId, name, description, startDate, finishDate);
        System.out.println("[OK]");
    }
}
