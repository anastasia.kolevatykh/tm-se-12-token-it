package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;

public final class JaxbJsonUnmarshallerCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "json-unmarshaller";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ju";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tLoad from json file JAX-B.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");
        System.out.println("[JSON UNMARSHALLER]");
        serviceLocator.getDomainEndpoint().unmarshalJaxbJson(token);
        System.out.println("[Loading is successful!]");
    }
}
