package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.constant.Message;
import ru.kolevatykh.tm.endpoint.Project;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pr";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected project with tasks.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");

        System.out.println("[" + getName().toUpperCase() + "]");
        System.out.println(Message.NAME);
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();
        if (id.isEmpty()) {
            throw new Exception("[The id can't be empty.]");
        }

        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectById(token, id);
        if (project == null) {
            throw new Exception("[The project '" + id + "' does not exist!]");
        }

        serviceLocator.getTaskEndpoint().removeProjectTasks(token, id);
        serviceLocator.getProjectEndpoint().removeProject(token, id);
        System.out.println("[" + getDescription() + "]\n" + Message.OK);
    }
}
