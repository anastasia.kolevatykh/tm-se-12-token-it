package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;

public final class JacksonLoadXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "jackson-xml-Load";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "xl";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tSave to xml file Jackson.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");
        System.out.println("[JACKSON XML LOAD]");
        serviceLocator.getDomainEndpoint().loadJacksonXml(token);
        System.out.println("[Saving is successful!]");
    }
}
