package ru.kolevatykh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import ru.kolevatykh.tm.util.PasswordHashUtil;

public final class UserEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ue";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tUpdate login or password.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");

        System.out.println("[USER EDIT]\nEnter your login for edit: ");
        @NotNull final String login = ConsoleInputUtil.getConsoleInput();
        if (login.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        System.out.println("Enter new login: ");
        @NotNull final String loginNew = ConsoleInputUtil.getConsoleInput();
        if (loginNew.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        System.out.println("Enter new password: ");
        @NotNull final String passwordNew = ConsoleInputUtil.getConsoleInput();
        if (passwordNew.isEmpty()) {
            throw new Exception("[The password can't be empty.]");
        }

        System.out.println("Enter new role type: ADMIN USER");
        @NotNull final String role = ConsoleInputUtil.getConsoleInput();
        if (role.isEmpty()) {
            throw new Exception("[The role can't be empty.]");
        }

        serviceLocator.getUserEndpoint().mergeUser(token, login, loginNew,
                PasswordHashUtil.getPasswordHash(passwordNew),
                role, true);
        System.out.println("[OK]");
    }
}
