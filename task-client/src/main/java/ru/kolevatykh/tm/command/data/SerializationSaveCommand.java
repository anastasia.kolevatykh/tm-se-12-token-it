package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;

public final class SerializationSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "save";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "s";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tSave data to file.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");
        System.out.println("[SAVE (serialization)]");
        serviceLocator.getDomainEndpoint().serialize(token);
        System.out.println("[Saving is successful!]");
    }
}
