package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;

public final class DeserializationLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "load";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "l";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tLoad data from file.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");
        System.out.println("[LOAD (deserialization)]");
        serviceLocator.getDomainEndpoint().deserialize(token);
        System.out.println("[Loading is successful!]");
    }
}
