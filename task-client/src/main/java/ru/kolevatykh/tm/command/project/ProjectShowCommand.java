package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.constant.Message;
import ru.kolevatykh.tm.endpoint.*;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import ru.kolevatykh.tm.wrapper.TaskWrapper;
import java.lang.Exception;
import java.util.List;

public final class ProjectShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-show";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "psh";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow tasks of selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");

        System.out.println("[" + getName().toUpperCase() + "]\n" + Message.NAME);
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();
        if (name.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectByName(token, name);
        if (project == null) {
            throw new Exception("[The project '" + name + "' does not exist!]");
        }

        @NotNull final String id = project.getId();
        @Nullable final List<Task> list = serviceLocator.getTaskEndpoint().findTasksByProjectId(token, id);
        if (list != null) {
            System.out.println("[TASK LIST]");
            @NotNull final StringBuilder tasks = new StringBuilder();
            int i = 0;

            for (@NotNull final Task task : list) {
                if (id.equals(task.getProjectId())) {
                    @NotNull final TaskWrapper taskWrapper = new TaskWrapper(task);
                    tasks
                            .append(++i)
                            .append(". ")
                            .append(taskWrapper.toString())
                            .append(System.lineSeparator());
                }
            }
            @NotNull final String taskString = tasks.toString();
            System.out.println(taskString);
        } else {
            System.out.println("[No tasks yet.]");
        }
    }
}
