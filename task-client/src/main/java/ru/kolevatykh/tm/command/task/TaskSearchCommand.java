package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Task;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import ru.kolevatykh.tm.wrapper.TaskWrapper;
import java.util.List;

public final class TaskSearchCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-search";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ts";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow all tasks that contains a word or phrase.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");

        System.out.println("[TASK SEARCH]\nEnter task search: ");
        @NotNull final String search = ConsoleInputUtil.getConsoleInput();
        if (search.isEmpty()) {
            throw new Exception("[The id can't be empty.]");
        }

        @Nullable final List<Task> list = serviceLocator.getTaskEndpoint().findTasksBySearch(token, search);
        if (list == null) {
            throw new Exception("[No tasks yet.]");
        }

        System.out.println("[TASK LIST]");
        @NotNull final StringBuilder tasks = new StringBuilder();
        int i = 0;
        for (@NotNull final Task task : list) {
            @NotNull final TaskWrapper taskWrapper = new TaskWrapper(task);
            tasks
                    .append(++i)
                    .append(". ")
                    .append(taskWrapper.toString())
                    .append(System.lineSeparator());
        }
        @NotNull final String taskString = tasks.toString();
        System.out.println(taskString);
    }
}
