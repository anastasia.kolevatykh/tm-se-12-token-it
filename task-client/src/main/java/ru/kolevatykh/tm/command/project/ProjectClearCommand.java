package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.constant.Message;

public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pcl";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects with tasks.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");
        System.out.println("[" + getName().toUpperCase() + "]");
        serviceLocator.getTaskEndpoint().removeTasksWithProjectId(token);
        serviceLocator.getProjectEndpoint().removeAllProjects(token);
        System.out.println("[" + getDescription() + "]\n" + Message.OK);
    }
}
