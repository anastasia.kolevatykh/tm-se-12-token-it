package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;

public final class JaxbXmlMarshallerCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "xml-marshaller";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "xm";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tSave all projects and tasks to xml file JAX-B.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");
        System.out.println("[XML MARSHALLER]");
        serviceLocator.getDomainEndpoint().marshalJaxbXml(token);
        System.out.println("[Saving is successful!]");
    }
}
