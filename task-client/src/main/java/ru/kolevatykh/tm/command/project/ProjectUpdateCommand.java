package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.constant.Message;
import ru.kolevatykh.tm.endpoint.Project;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

public final class ProjectUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pu";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");

        System.out.println("[" + getName().toUpperCase() + "]\n" + Message.ID);
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();
        if (id.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectById(token, id);
        if (project == null) {
            throw new Exception("[The project '" + id + "' does not exist!]");
        }

        System.out.println(Message.NAME);
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();

        System.out.println(Message.DESCRIPTION);
        @NotNull final String description = ConsoleInputUtil.getConsoleInput();

        System.out.println(Message.START_DATE);
        @NotNull final String startDate = ConsoleInputUtil.getConsoleInput();

        System.out.println(Message.FINISH_DATE);
        @NotNull final String finishDate = ConsoleInputUtil.getConsoleInput();

        serviceLocator.getProjectEndpoint().mergeProject(token, id, name, description, startDate, finishDate);
        System.out.println(Message.OK);
    }
}
