package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.constant.Message;

public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tcl";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tRemove all tasks.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");
        System.out.println("[" + getName().toUpperCase() + "]");
        serviceLocator.getTaskEndpoint().removeAllTasks(token);
        System.out.println("[" + getDescription() + "]");
        System.out.println(Message.OK);
    }
}
