package ru.kolevatykh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

public final class UserDeleteCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-delete";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ud";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tDelete account.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER DELETE ACCOUNT]");
        System.out.println("[Are you sure you want to delete your account? y/n]");
        @NotNull final String answer = ConsoleInputUtil.getConsoleInput();

        if (answer.equals("y")) {
            System.out.println("Enter login to confirm removal: ");
            @NotNull final String login = ConsoleInputUtil.getConsoleInput();
            if (login.isEmpty()) {
                throw new Exception("[The name can't be empty.]");
            }

            @Nullable final String token = serviceLocator.getToken();
            if (token == null) {
                throw new Exception("[The user '" + login + "' does not exist!]");
            }

            serviceLocator.getTaskEndpoint().removeAllTasks(token);
            serviceLocator.getProjectEndpoint().removeAllProjects(token);
            serviceLocator.getUserEndpoint().removeUser(token);
            serviceLocator.setToken(null);
            System.out.println("[Deleted user account with projects and tasks.]\n[OK]");
        }
    }
}
