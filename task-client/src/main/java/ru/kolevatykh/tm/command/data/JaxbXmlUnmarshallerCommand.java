package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;

public final class JaxbXmlUnmarshallerCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "xml-unmarshaller";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "xu";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tLoad from xml file JAX-B.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");
        System.out.println("[XML UNMARSHALLER]");
        serviceLocator.getDomainEndpoint().unmarshalJaxbXml(token);
        System.out.println("[Loading is successful!]");
    }
}
