package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;

public final class JacksonSaveJsonCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "jackson-json-save";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "js";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tSave to json file Jackson.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");
        System.out.println("[JACKSON JSON SAVE]");
        serviceLocator.getDomainEndpoint().saveJacksonJson(token);
        System.out.println("[Saving is successful!]");
    }
}
