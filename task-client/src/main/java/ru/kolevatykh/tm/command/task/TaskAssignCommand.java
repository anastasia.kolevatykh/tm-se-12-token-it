package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.constant.Message;
import ru.kolevatykh.tm.endpoint.*;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import java.lang.Exception;

public final class TaskAssignCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-assign";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ta";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tAssign task to project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");

        System.out.println("[" + getName().toUpperCase() + "]");
        System.out.println("Enter task id for assignment:");
        @NotNull final String taskId = ConsoleInputUtil.getConsoleInput();
        if (taskId.isEmpty()) {
            throw new Exception("[The task id can't be empty.]");
        }

        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskById(token, taskId);
        if (task == null) {
            throw new Exception("[The task id '" + taskId + "' does not exist!]");
        }

        System.out.println("Enter project id to assign task to:");
        @NotNull final String projectId = ConsoleInputUtil.getConsoleInput();
        if (projectId.isEmpty()) {
            throw new Exception("[The project id can't be empty.]");
        }

        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectById(token, projectId);
        if (project == null) {
            throw new Exception("[The project id '" + projectId + "' does not exist!]");
        }

        serviceLocator.getTaskEndpoint().assignToProject(token, taskId, projectId);
        System.out.println(Message.OK);
    }
}
