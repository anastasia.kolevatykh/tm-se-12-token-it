package ru.kolevatykh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.User;
import ru.kolevatykh.tm.wrapper.UserWrapper;
import java.util.List;

public final class UserListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-list";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "uls";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow all users.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");

        System.out.println("[USER LIST]");
        @NotNull final List<User> userList = serviceLocator.getUserEndpoint().findAllUsers(token);
        if (userList.isEmpty()) {
            throw new Exception("[No users yet.]");
        }

        @NotNull final StringBuilder users = new StringBuilder();
        int i = 0;

        for (@NotNull final User user : userList) {
            @NotNull final UserWrapper userWrapper = new UserWrapper(user);
            users
                    .append(++i)
                    .append(". ")
                    .append(userWrapper.toString())
                    .append(System.lineSeparator());
        }

        @NotNull final String userString = users.toString();
        System.out.println(userString);
    }
}
