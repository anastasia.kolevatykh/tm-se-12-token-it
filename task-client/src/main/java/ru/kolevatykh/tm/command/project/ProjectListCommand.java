package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Project;
import ru.kolevatykh.tm.wrapper.ProjectWrapper;
import java.util.List;

public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pls";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow all projects.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");

        System.out.println("[" + getName().toUpperCase() + "]");
        @Nullable final List<Project> list = serviceLocator.getProjectEndpoint().findAllProjectsByUserId(token);
        if (list == null) {
            throw new Exception("[No projects yet.]");
        }

        @NotNull final StringBuilder projects = new StringBuilder();
        int i = 0;
        for (@NotNull final Project project : list) {
            @NotNull final ProjectWrapper projectWrapper = new ProjectWrapper(project);
            projects.append(++i)
                    .append(". ")
                    .append(projectWrapper.toString())
                    .append(System.lineSeparator());
        }
        @NotNull final String projectString = projects.toString();
        System.out.println(projectString);
    }
}
