package ru.kolevatykh.tm.command.general;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\t\tShow all commands.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }@Override
    public void execute() {
        for (@NotNull final AbstractCommand command : serviceLocator.getCommands()) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }
}
