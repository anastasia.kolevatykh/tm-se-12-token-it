package ru.kolevatykh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.*;
import ru.kolevatykh.tm.wrapper.ProjectWrapper;
import ru.kolevatykh.tm.wrapper.TaskWrapper;
import java.lang.Exception;
import java.util.List;

public final class UserShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-show";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ush";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow all projects and tasks.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        if (token == null) throw new Exception("[The token is absent.]");

        System.out.println("[USER SHOW]");
        @Nullable final User user = serviceLocator.getTokenEndpoint().getUserByToken(token);
        if (user == null) {
            throw new Exception("[The user does not exist!]");
        }

        System.out.println("[LOGIN]");
        System.out.println(user.getLogin());
        System.out.println("[ROLE]");
        System.out.println(user.getRoleType());

        @Nullable final List<Project> projectList = serviceLocator.getProjectEndpoint().findAllProjectsByUserId(token);

        if (projectList != null) {
            System.out.println("[PROJECT LIST]");
            int projectCount = 0;

            for (@NotNull final Project project : projectList) {
                @Nullable final List<Task> taskList = serviceLocator.getTaskEndpoint()
                        .findTasksByProjectId(token, project.getId());

                @NotNull final ProjectWrapper projectWrapper = new ProjectWrapper(project);
                System.out.println(++projectCount + ". " + projectWrapper.toString());

                if (taskList != null) {
                    System.out.println("[TASK LIST]");
                    @Nullable final StringBuilder projectTasks = new StringBuilder();
                    int taskCount = 0;

                    for (@NotNull final Task task : taskList) {
                        @NotNull final TaskWrapper taskWrapper = new TaskWrapper(task);
                        projectTasks
                                .append(projectCount)
                                .append(".")
                                .append(++taskCount)
                                .append(". ")
                                .append(taskWrapper.toString())
                                .append("\n");
                    }

                    @NotNull final String taskString = projectTasks.toString();
                    System.out.println(taskString);
                } else {
                    System.out.println("[No tasks yet.]");
                }
            }
        }

        @Nullable final List<Task> taskNotAssignedList = serviceLocator.getTaskEndpoint().findTasksWithoutProject(token);

        if (taskNotAssignedList != null) {
            System.out.println("[NOT ASSIGNED TASKS]");
            @NotNull final StringBuilder taskNotAssigned = new StringBuilder();
            int taskCount = 0;

            for (@NotNull final Task task : taskNotAssignedList) {
                @NotNull final TaskWrapper taskWrapper = new TaskWrapper(task);
                taskNotAssigned
                        .append(++taskCount)
                        .append(". ")
                        .append(taskWrapper.toString())
                        .append(System.lineSeparator());
            }

            @NotNull final String taskString = taskNotAssigned.toString();
            System.out.println(taskString);
        }

        if (projectList == null & taskNotAssignedList == null) {
            throw new Exception("[No projects and tasks yet.]");
        }
    }
}
