package ru.kolevatykh.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.constant.Message;
import ru.kolevatykh.tm.endpoint.*;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import java.lang.Exception;
import java.util.*;

@Setter
@Getter
@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @Nullable
    private String token;

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpointService().getDomainEndpointPort();

    @NotNull
    private final ITokenEndpoint tokenEndpoint = new TokenEndpointService().getTokenEndpointPort();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.kolevatykh.tm").getSubTypesOf(AbstractCommand.class);

    private void registryCommand(@NotNull final AbstractCommand command) throws Exception {
        @NotNull final String commandName = command.getName();
        @NotNull final String commandShortName = command.getShortName();
        @NotNull final String commandDescription = command.getDescription();

        if (commandName.isEmpty()) {
            throw new Exception("[There's no such command name.]");
        }
        if (commandDescription.isEmpty()) {
            throw new Exception("[There's no such command description.]");
        }
        command.setServiceLocator(this);
        commands.put(commandName, command);

        if (!commandShortName.isEmpty()) {
            commands.put(commandShortName, command);
        }
    }

    private void start() {
        System.out.println(Message.WELCOME);
        @NotNull String command = "";
        while (!"exit".equals(command)) {
            command = ConsoleInputUtil.getConsoleInput();
            try {
                execute(command);
            } catch (Exception e) {
                if (e.getMessage().equals("[The session is timed out.]")
                        || e.getMessage().equals("[Wrong token signature.]")
                        || e.getMessage().equals("[The token is absent.]")) {
                    setToken(null);
                    System.out.println("[Please log in or register.]");
                }
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(@NotNull final String command) throws Exception {
        if (command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void init() {
        try {
            for (@NotNull final Class<?> classItem : classes) {
                registryCommand((AbstractCommand) classItem.newInstance());
            }
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
