package ru.kolevatykh.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class Message {

    @NotNull public static final String WELCOME = "*** Welcome to task manager ***"
            + System.lineSeparator() + "[Type 'help' for details.]";

    @NotNull public static final String ID = "Enter id: ";

    @NotNull public static final String NAME = "Enter name: ";

    @NotNull public static final String DESCRIPTION = "Enter description: ";

    @NotNull public static final String START_DATE = "Enter start date, for instance 30.3.2020: ";

    @NotNull public static final String FINISH_DATE = "Enter finish date, for instance 30.3.2020: ";

    @NotNull public static final String STATUS = "Enter new status name: PLANNED INPROCESS READY";

    @NotNull public static final String OK = "[OK]";
}
